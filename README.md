# gaia-challenge

> Coding Challenge

## DEMO Setup

- Requirements: NodeJS, npm

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```

## Completed
- Seeking Truth section + routing placeholders
- Page Header (Large image, etc.)
- Video Title grid
- Sorting
- 'LOAD MORE' paging + back-to-top
- Title Cards
    - Images
    - Title
    - Season, Episode AND Seasons, Episodes
    - Runtime
    - MORE Descriptions for each title
- Responsiveness: Two sizes default and <520px-width

## TO-DO
- Search functionality
- Modularization of Request/REST handlers
- VUEX integration 
    - one-way + "immutable"
    - removal of unnecessary signal handling
    - general data tree organization
- Testing
    - Mocha: Mimimum = Unit tests
- Responsiveness
    - More sizes(at least 4)
    - Landscape
    - Common cellphone/device pixel density handling
    - Additional functionality if required
- CSS
    - ensure DRYness
    - BEM (if/where necessary)
    - reset + other conventional files
    - use of multiple image sizes
- Code review and general cleanup
- Cross-browser fixup
- Other major features
    - Site sections + features
    - User accounts, auth
    - etc.


