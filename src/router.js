import Vue from 'vue';
import VueRouter from 'vue-router';

import Centers from '@/components/views/Centers';
import Films from '@/components/views/Films';
import Home from '@/components/views/Home';
import MyGaia from '@/components/views/MyGaia';
import Transformation from '@/components/views/Transformation';
import Truth from '@/components/views/Truth';
import Yoga from '@/components/views/Yoga';

Vue.use(VueRouter);

/* eslint-disable no-new, new-cap */
export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/centers',
      name: 'Centers',
      component: Centers,
    },
    {
      path: '/films',
      name: 'Films',
      component: Films,
    },
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/my-gaia',
      name: 'MyGaia',
      component: MyGaia,
    },
    {
      path: '/transformation',
      name: 'Transformation',
      component: Transformation,
    },
    {
      path: '/truth',
      name: 'Truth',
      component: Truth,
    },
    {
      path: '/yoga',
      name: 'Yoga',
      component: Yoga,
    },
  ],
});
