import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

/* eslint-disable no-new, new-cap */
export default new Vuex.Store({
  modules: {},
  strict: true,
});

